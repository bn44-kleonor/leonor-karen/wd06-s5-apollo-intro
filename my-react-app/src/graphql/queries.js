import { gql } from 'apollo-boost';

const getMembersQuery = gql`
{
	members {
		id
		firstName
		lastName
		position
		team {
			name
		}
	}
}`

const getTeamsQuery = gql`
{
	teams{
		id
		name
	}
}`

export { getMembersQuery, getTeamsQuery };