import React, { useState } from 'react';

const TeamRow = (props) => {
	return (
			<tr key={props.team._id}>
				<td>{props.team.name}</td>
				<td>{props.team.action}</td>
			</tr>
	)
}

export default TeamRow;
