import React, { useState, useEffect } from 'react';
// 8. Import React's prop-types
import PropTypes from 'prop-types';
import 'react-bulma-components/dist/react-bulma-components.min.css';
import {
	Card
} from 'react-bulma-components';
import TeamRow from './TeamRow';

const TeamList = (props) => {

	let rows = "";
	if(props.teams.length === 0) {
		rows = (
			<tr>
				<td colSpan="4">
					<em>No Teams Found</em>
				</td>
			</tr>
		)
	} else {
		rows = props.teams.map(team=>{
			return <TeamRow team={team}/>
		})
	}

	return (
			<Card>
				<Card.Header>
					<Card.Header.Title>
						Team List
					</Card.Header.Title>
				</Card.Header>
				<Card.Content>
					<table className="table is-fullwidth is-bordered is-striped is-hoverable">
						<thead>
							<tr>
								<th>Team Name</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
							{rows}
						</tbody>
					</table>
				</Card.Content>
			</Card>
		)
}

TeamList.propTypes = {
	teams:PropTypes.array
}

export default TeamList;
