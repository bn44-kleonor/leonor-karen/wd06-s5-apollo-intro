import React, { useState, useEffect } from 'react';
// 8.Import React's prop-types
import PropTypes from 'prop-types';
import 'react-bulma-components/dist/react-bulma-components.min.css';
import {
    Card
} from 'react-bulma-components';
import MemberRow from './MemberRow';
 
const MemberList = (props) => {
 
        let rows = "";
        if(typeof props.members == "undefined" || props.members.length === 0){
                rows = (
                        <tr>
                          <td colSpan="4">
                            <em>No Members Found</em>
                          </td>
                        </tr>
                )
        } else {
                rows = props.members.map(member=>{
                        return <MemberRow member={member}/>
                })
        }
   
        return (
                <Card>
                    <Card.Header>
                        <Card.Header.Title>
                          Member List
                        </Card.Header.Title>
                    </Card.Header>
                        <Card.Content>
                         <table className="table is-fullwidth is-bordered is-striped is-hoverable">
                            <thead>
                               <tr>
                                 <th>Member Name</th>
                                 <th>Position</th>
                                 <th>Team</th>
                                 <th>Action</th>
                               </tr>
                            </thead>
                            <tbody>
                                {/* 7) Emply conditional rendering  */
                                   /*
                                   (props.members.length === 0)? (
                                           <tr>
                                                   <td colSpan="4">
                                                                <em>No Members Found.</em>
                                                   </td>
                                           </tr>
                                   ):
                                   (
                                        props.members.map(member => {
                                                return <MemberRow x={member}/>
                                        })
                                   )
                                */}
                                                                           
                                    {rows}
                           
                            </tbody>
                         </table>
                        </Card.Content>
                </Card>
        )
}
// 9. Apply prop type checkng in component
 
MemberList.propTypes = {
        members:PropTypes.array
}
export default MemberList;
