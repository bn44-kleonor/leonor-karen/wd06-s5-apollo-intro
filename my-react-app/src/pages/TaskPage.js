import React, { useState } from 'react';
import 'react-bulma-components/dist/react-bulma-components.min.css';

import TaskAdd from '../components/TaskAdd';
import TaskList from '../components/TaskList';

import {
	Section,
	Heading,
	Columns
} from 'react-bulma-components';

const TaskPage = (props) => {
	/* 1.) Create state (i.e., Single Source of TRUTH */ 
	// console.log(useState());	//returns 2 elements: undefined, function
	// console.log(useState('hello'));
	const[tasks, setTasks] = useState([]); //assign the elements: tasks(undefined), setTasks(function)
											//setTasks -> the function you will call when you want to make changes to 'tasks'
											//setTasks -> the single source of truth (since it is in the parent component then it is accessible to its children via props)

	/* 2.) Create a new function to add a document (e.g. add Task) 
		- we can pass this as props to our child add Task component
	*/ 
	const addTask = (newTask) => {			//function 'addTask' ay ipapasa mamaya sa child component
		setTasks([...tasks,newTask]);		//
	}

	const sectionStyle = {
		paddingTop: "3em",
		paddingBottom: '3em'
	};

	return (
			<Section size="medium" style={ sectionStyle }>
				<Heading>Tasks</Heading>
				<Columns>
					<Columns.Column size={4}>
						{/* 3) Pass the new function as prop to the child component
							addTask is the props
							{addTask} is the function created at step no. 2
						*/}
						<TaskAdd addTask={addTask} />
					</Columns.Column>

					<Columns.Column>
						{/* 6) Pass props to second child*/}
						<TaskList tasks={tasks}/>
					</Columns.Column>
				</Columns>
			</Section>
	)
}

export default TaskPage;
