import React, { Component } from 'react';
import 'react-bulma-components/dist/react-bulma-components.min.css';
import {
	Section
} from 'react-bulma-components';

class NotFound extends Component {
	render()
	{
		const sectionStyle = {
			paddingTop: "3em",
			paddingBottom: '3em'
		};

		return (
			<Section size="medium" style={ sectionStyle }>
				<p>Page not found abc</p>
			</Section>
		)
	}
}

export default NotFound;
