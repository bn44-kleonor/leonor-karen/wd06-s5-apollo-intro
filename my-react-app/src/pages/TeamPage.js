import React, { useState } from 'react';
import 'react-bulma-components/dist/react-bulma-components.min.css';

import TeamAdd from '../components/TeamAdd';
import TeamList from '../components/TeamList';

import {
	Section,
	Heading,
	Columns
} from 'react-bulma-components';

import { graphql } from 'react-apollo';
import { getTasksQuery } from '../graphql/queries';

const TeamPage = (props) => {

	/*1.) Create state (i.e., Single Source of Truth)*/
	const[teams,setTeams] = useState([]);

	/* 2.) Create a new function to add a document (e.g. add Team)
		- we can pass this as props to our child add Team component
	*/
	const addTeam = (newTeam) => {
		setTeams([...teams,newTeam]);
	}
	const sectionStyle = {
		paddingTop: "3em",
		paddingBottom: '3em'
	};

	return (
		<Section size="medium" style={ sectionStyle }>
			<Heading>Teams</Heading>
			<Columns>
				<Columns.Column size={4}>
				{/* 3) Pass the new function as prop to the child component*/}
					<TeamAdd addTeam={addTeam} />
				</Columns.Column>
				<Columns.Column>
				{/* 6) pass props to second child*/}
					<TeamList teams={teams}/>
				</Columns.Column>
			</Columns>
		</Section>
	)
}

export default TeamPage;
